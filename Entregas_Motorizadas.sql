---Creacion tabla Cliente
CREATE TABLE cliente(
cod_cliente varchar(10) PRIMARY KEY not null,
cedula_cliente varchar(10) not null,
nombres_cliente varchar(30) not null,
apellidos_cliente varchar(40) not null,
fecha_nacimiento_cliente date not null,
direccion_cliente varchar(30) not null,
celular_cliente varchar(10) not null);
---Creacion tabla Area
CREATE TABLE area(
cod_area varchar(10) PRIMARY KEY not null,
nombre_area varchar(20) not null,
calle varchar(30) not null,
avenida varchar(40) not null);
---Creacion tabla Personal_Administrativo
CREATE TABLE personal_administrativo(
cod_administrativo varchar(10) PRIMARY KEY not null,
cedula_administrativo varchar(10) not null,
nombres_administrativo varchar(30) not null,
apellidos_administrativo varchar(40) not null,
direccion_administrativo varchar (40) not null,
edad_administrativo varchar(2) not null,
celular_administrativo varchar(10) not null);
---Creacion tabla Moto
CREATE TABLE moto(
cod_moto varchar(10) PRIMARY KEY not null,
marca varchar(20) not null,
color varchar(10) not null,
cilindrada varchar(10) not null,
marca_motor varchar(15) not null);
---Creacion tabla Personal_Motorizado
CREATE TABLE personal_motorizado(
cod_motorizado varchar(10) PRIMARY KEY not null,
cod_moto varchar(10) not null,
cedula_motorizado varchar(10) not null,
nombres_motorizado varchar(30) not null,
apellidos_motorizado varchar(40) not null,
celular_motorizado varchar(10) not null,
tipo_licencia varchar(1) not null,
expedicion_licencia date not null);
---Creacion tabla Revision
CREATE TABLE revision(
cod_revision varchar(10) PRIMARY KEY not null,
cod_motorizado varchar(10) not null,
cod_moto varchar(10) not null,
estado_moto varchar(10) not null,
reparaciones_moto numeric not null,
fecha_revision date not null,
FOREIGN KEY (cod_motorizado) REFERENCES personal_motorizado(cod_motorizado) on delete restrict on update restrict,
FOREIGN KEY (cod_moto) REFERENCES moto(cod_moto) on delete restrict on update restrict);
---Creacion tabla Solicitud
CREATE TABLE solicitud(
cod_solicitud varchar(10) PRIMARY KEY not null,
cod_cliente varchar(10) not null,
cod_administrativo varchar(10) not null,
cod_area varchar(10) not null,
fecha_solicitud date not null,
estado_solicitud varchar(10) not null,
categoria_productos varchar(15) not null,
cantidad_productos decimal not null,
FOREIGN KEY (cod_cliente) REFERENCES cliente(cod_cliente) on delete restrict on update restrict,
FOREIGN KEY (cod_administrativo) REFERENCES personal_administrativo(cod_administrativo) on delete restrict on update restrict,
FOREIGN KEY (cod_area) REFERENCES area(cod_area) on delete restrict on update restrict);
---Creacion tabla Entrega
CREATE TABLE entrega(
cod_entrega varchar(10) PRIMARY KEY not null,
cod_solicitud varchar(10) not null,
cod_area varchar(10) not null,
cod_motorizado varchar(10) not null,
cod_cliente varchar(10) not null,
fecha_entrega date not null,
estado_entrega varchar(10) not null,
peso_entrega decimal not null,
FOREIGN KEY (cod_solicitud) REFERENCES solicitud(cod_solicitud) on delete restrict on update restrict,
FOREIGN KEY (cod_area) REFERENCES area(cod_area) on delete restrict on update restrict,
FOREIGN KEY (cod_motorizado) REFERENCES personal_motorizado(cod_motorizado) on delete restrict on update restrict,
FOREIGN KEY (cod_cliente) REFERENCES cliente(cod_cliente) on delete restrict on update restrict);
---Creacion tabla Factura
CREATE TABLE factura(
cod_factura varchar(10) PRIMARY KEY not null,
cod_entrega varchar(10) not null,
subtotal decimal not null,
comision decimal not null,
iva decimal not null,
total decimal not null,
FOREIGN KEY (cod_entrega) REFERENCES entrega(cod_entrega) on delete restrict on update restrict);






---Ingreso de datos tabla Cliente
INSERT INTO cliente VALUES('01','1315661726','Jesus Smir','Toala Delgado','2001/08/01','Calle 8 Avenida 28','0968959182');
INSERT INTO cliente VALUES('02','1308251709','Emiliano Daniel','Soledispa Catagua','1995/07/15','Calle 13 Avenida 15','0995918565');
INSERT INTO cliente VALUES('03','1305965218','Carlos Cesar','Baque Velez','2000/12/25','Calle 112 Avenida 124','0998565231');
INSERT INTO cliente VALUES('04','1319845301','Javier Frank','Insua Menoscal','1989/02/15','Calle 18 Avenida 30','0984532100');
INSERT INTO cliente VALUES('05','1305963175','Francisco Lider','Anchundia Piguave','2002/05/30','Calle 103 Avenida 109','0995412363');
---Ingreso de datos tabla Area
INSERT INTO area VALUES('01','La Dolorosa','Calle 9','Avenida 22');
INSERT INTO area VALUES('02','Santa Martha','Calle 12','Avenida 30');
INSERT INTO area VALUES('03','Los Esteros','Calle 110','Avenida 114');
INSERT INTO area VALUES('04','Santa Monica','Calle 19','Avenida 26');
--- Ingreso de datos tabla Personal_Administrativo
INSERT INTO personal_administrativo VALUES('01','1305961238','Teresa Belen','Bustamante Pin','Calle 13 Avenida 26','28','0996512020');
INSERT INTO personal_administrativo VALUES('02','1315963201','Sebastian Gregorio','Torres Palacios','Calle 30 Avenida 9','39','0989541239');
INSERT INTO personal_administrativo VALUES('03','1305961238','Maria Jose','Santana Mendoza','Calle 7 Avenida 17','36','0995156314');
---Ingreso de datos tabla Moto
INSERT INTO moto VALUES('01','Honda','Roja','250','Mash');
INSERT INTO moto VALUES('02','Suzuki','Negra','125','Mash');
INSERT INTO moto VALUES('03','Kawasaki','Amarilla','300','Mash');
INSERT INTO moto VALUES('04','Daytona','Negra','250','Mash');
INSERT INTO moto VALUES('05','Honda','Azul','180','Mash');
---Ingreso de datos tabla Personal_Motorizado
INSERT INTO personal_motorizado VALUES('01','05','1309562136','Franklin Jose','Bustamante Vargas','0954852132','C','2021/10/30');
INSERT INTO personal_motorizado VALUES('02','04','1315623165','Jose Edwin','Perlaza Quininde','0995412956','C','2021/10/23');
INSERT INTO personal_motorizado VALUES('03','03','1304530121','Erwin Erick','Peralta Vinces','0989651202','C','2021/11/12');
INSERT INTO personal_motorizado VALUES('04','02','1305478962','Byron Octavio','Quimiz Salvatierra','0961203678','C','2022/10/13');
INSERT INTO personal_motorizado VALUES('05','01','1315478963','Leonel Jonathan','Cruz Martinez','0984123648','C','2022/04/09');
---Ingreso de datos tabla Revision
INSERT INTO revision VALUES('01','01','05','Bueno','0','2021/05/30');
INSERT INTO revision VALUES('02','02','04','Dañada','1','2021/05/30');
INSERT INTO revision VALUES('03','03','03','Bueno','0','2021/05/30');
INSERT INTO revision VALUES('04','04','02','Dañada','2','2021/05/30');
INSERT INTO revision VALUES('05','05','01','Dañada','2','2021/05/30');
INSERT INTO revision VALUES('06','01','05','Bueno','1','2021/08/27');
INSERT INTO revision VALUES('07','02','04','Dañada','3','2021/08/27');
INSERT INTO revision VALUES('08','03','03','Bueno','0','2021/08/27');
INSERT INTO revision VALUES('09','04','02','Bueno','0','2021/08/27');
INSERT INTO revision VALUES('10','05','01','Dañada','2','2021/08/27');
---Ingreso de datos tabla Solicitud
INSERT INTO solicitud VALUES('01','02','02','04','2020/12/12','Rechazado','Desconocido','0');
INSERT INTO solicitud VALUES('02','04','03','03','2020/12/25','Rechazado','Desconocido','0');
INSERT INTO solicitud VALUES('03','04','02','02','2021/01/25','Confirmado','Ropa','1');
INSERT INTO solicitud VALUES('04','02','01','03','2021/01/29','Confirmado','Alimentos','2');
INSERT INTO solicitud VALUES('05','01','03','01','2021/02/13','Rechazado','Desconocido','0');
INSERT INTO solicitud VALUES('06','03','03','01','2021/02/25','Confirmado','Alimento','3');
INSERT INTO solicitud VALUES('07','05','01','04','2021/03/10','Confirmado','Bebida','9');
INSERT INTO solicitud VALUES('08','02','02','03','2021/03/28','Rechazado','Desconocido','0');
INSERT INTO solicitud VALUES('09','03','02','01','2021/03/31','Confirmado','Bebidas','3');
INSERT INTO solicitud VALUES('10','04','02','02','2021/04/05','Confirmado','Ropa','1');
INSERT INTO solicitud VALUES('11','05','01','03','2021/04/17','Confirmado','Alimento','5');
---Ingreso de datos tabla Entrega
INSERT INTO entrega VALUES('01','03','04','02','04','2021/01/25','Entregado','1');
INSERT INTO entrega VALUES('02','04','03','05','02','2021/01/29','Entregado','2');
INSERT INTO entrega VALUES('03','06','01','03','03','2021/02/25','Entregado','1');
INSERT INTO entrega VALUES('04','07','04','01','05','2021/03/10','Entregado','1');
INSERT INTO entrega VALUES('05','09','01','04','03','2021/03/31','Entregado','2');
INSERT INTO entrega VALUES('06','10','02','04','04','2021/04/05','Entregado','1');
INSERT INTO entrega VALUES('07','11','03','02','05','2021/04/17','Entregado','1');
---Ingreso de datos tabla Factura
INSERT INTO factura VALUES('01','01','4.00','3.00','0.48','7.48');
INSERT INTO factura VALUES('02','02','8.00','3.00','0.96','11.96');
INSERT INTO factura VALUES('03','03','4.00','3.00','0.48','7.48');
INSERT INTO factura VALUES('04','04','4.00','3.00','0.48','7.48');
INSERT INTO factura VALUES('05','05','8.00','3.00','0.96','11.96');
INSERT INTO factura VALUES('06','06','4.00','3.00','0.48','7.48');
INSERT INTO factura VALUES('07','07','4.00','3.00','0.48','7.48');

---Primera Consulta-Historico de entregas motorizadas
select
personal_motorizado.cod_motorizado as Codigo_Motorizado,
personal_motorizado.nombres_motorizado as Nombres,
personal_motorizado.apellidos_motorizado as Apellidos,
extract (year from entrega.fecha_entrega) as Fecha,
count (personal_motorizado.cod_motorizado) as Numero_Entregas
from entrega
inner join personal_motorizado on entrega.cod_motorizado=personal_motorizado.cod_motorizado
group by extract (year from entrega.fecha_entrega), personal_motorizado.cod_motorizado,
personal_motorizado.nombres_motorizado, personal_motorizado.apellidos_motorizado
order by personal_motorizado.cod_motorizado asc

---Segunda Consulta- Historico de dinero recaudado
select
entrega.cod_area as Codigo_Area,
area.nombre_area as Nombre_Area,
sum (factura.comision) as Comision,
extract (year from entrega.fecha_entrega) as Fecha
from factura
inner join entrega on factura.cod_entrega=entrega.cod_entrega
inner join area on entrega.cod_area=area.cod_area
group by extract (year from entrega.fecha_entrega),
entrega.cod_area, area.nombre_area, factura.comision 
order by entrega.cod_area asc


---Tercer Consulta- Historico de unidades dañadas
select
extract (year from revision.fecha_revision) as Fecha,
personal_motorizado.nombres_motorizado as Nombres,
personal_motorizado.apellidos_motorizado as Apellidos,
moto.cod_moto as Codigo_moto,
count (moto.cod_moto) as Veces_Dañadas
from revision 
inner join personal_motorizado on revision.cod_motorizado=personal_motorizado.cod_motorizado
inner join moto on revision.cod_moto=moto.cod_moto
where revision.estado_moto = 'Dañada'
group by extract (year from revision.fecha_revision), personal_motorizado.nombres_motorizado,
personal_motorizado.apellidos_motorizado, moto.cod_moto
order by moto.cod_moto asc


---Cuarta Cosnulta- Historico de solicitudes de clientes
select
cliente.nombres_cliente as Nombres,
cliente.apellidos_cliente as Apellidos,
extract (year from solicitud.fecha_solicitud) as Fecha,
count (cliente.nombres_cliente) as Numero_Solicitudes
from solicitud
inner join cliente on solicitud.cod_cliente=cliente.cod_cliente
group by extract (year from solicitud.fecha_solicitud), cliente.nombres_cliente, cliente.apellidos_cliente