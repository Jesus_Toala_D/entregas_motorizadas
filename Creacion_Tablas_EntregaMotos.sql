---Creacion tabla Cliente
CREATE TABLE cliente(
cod_cliente varchar(10) PRIMARY KEY not null,
cedula_cliente varchar(10) not null,
nombres_cliente varchar(30) not null,
apellidos_cliente varchar(40) not null,
fecha_nacimiento_cliente date not null,
direccion_cliente varchar(30) not null,
celular_cliente varchar(10) not null);
---Creacion tabla Area
CREATE TABLE area(
cod_area varchar(10) PRIMARY KEY not null,
nombre_area varchar(20) not null,
calle varchar(30) not null,
avenida varchar(40) not null);
---Creacion tabla Personal_Administrativo
CREATE TABLE personal_administrativo(
cod_administrativo varchar(10) PRIMARY KEY not null,
cedula_administrativo varchar(10) not null,
nombres_administrativo varchar(30) not null,
apellidos_administrativo varchar(40) not null,
direccion_administrativo varchar (40) not null,
edad_administrativo varchar(2) not null,
celular_administrativo varchar(10) not null);
---Creacion tabla Moto
CREATE TABLE moto(
cod_moto varchar(10) PRIMARY KEY not null,
marca varchar(20) not null,
color varchar(10) not null,
cilindrada varchar(10) not null,
marca_motor varchar(15) not null);
---Creacion tabla Personal_Motorizado
CREATE TABLE personal_motorizado(
cod_motorizado varchar(10) PRIMARY KEY not null,
cod_moto varchar(10) not null,
cedula_motorizado varchar(10) not null,
nombres_motorizado varchar(30) not null,
apellidos_motorizado varchar(40) not null,
celular_motorizado varchar(10) not null,
tipo_licencia varchar(1) not null,
expedicion_licencia date not null);
---Creacion tabla Revision
CREATE TABLE revision(
cod_revision varchar(10) PRIMARY KEY not null,
cod_motorizado varchar(10) not null,
cod_moto varchar(10) not null,
estado_moto varchar(10) not null,
reparaciones_moto numeric not null,
fecha_revision date not null,
FOREIGN KEY (cod_motorizado) REFERENCES personal_motorizado(cod_motorizado) on delete restrict on update restrict,
FOREIGN KEY (cod_moto) REFERENCES moto(cod_moto) on delete restrict on update restrict);
---Creacion tabla Solicitud
CREATE TABLE solicitud(
cod_solicitud varchar(10) PRIMARY KEY not null,
cod_cliente varchar(10) not null,
cod_administrativo varchar(10) not null,
cod_area varchar(10) not null,
fecha_solicitud date not null,
estado_solicitud varchar(10) not null,
categoria_productos varchar(15) not null,
cantidad_productos decimal not null,
FOREIGN KEY (cod_cliente) REFERENCES cliente(cod_cliente) on delete restrict on update restrict,
FOREIGN KEY (cod_administrativo) REFERENCES personal_administrativo(cod_administrativo) on delete restrict on update restrict,
FOREIGN KEY (cod_area) REFERENCES area(cod_area) on delete restrict on update restrict);
---Creacion tabla Entrega
CREATE TABLE entrega(
cod_entrega varchar(10) PRIMARY KEY not null,
cod_solicitud varchar(10) not null,
cod_area varchar(10) not null,
cod_motorizado varchar(10) not null,
cod_cliente varchar(10) not null,
fecha_entrega date not null,
estado_entrega varchar(10) not null,
peso_entrega decimal not null,
FOREIGN KEY (cod_solicitud) REFERENCES solicitud(cod_solicitud) on delete restrict on update restrict,
FOREIGN KEY (cod_area) REFERENCES area(cod_area) on delete restrict on update restrict,
FOREIGN KEY (cod_motorizado) REFERENCES personal_motorizado(cod_motorizado) on delete restrict on update restrict,
FOREIGN KEY (cod_cliente) REFERENCES cliente(cod_cliente) on delete restrict on update restrict);
---Creacion tabla Factura
CREATE TABLE factura(
cod_factura varchar(10) PRIMARY KEY not null,
cod_entrega varchar(10) not null,
subtotal decimal not null,
comision decimal not null,
iva decimal not null,
total decimal not null,
FOREIGN KEY (cod_entrega) REFERENCES entrega(cod_entrega) on delete restrict on update restrict);